package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

var (
	zsrcSyncName string = "ZRCSyncSrv"
	zsrcSyncPath string = `C:\Zone Soft\ZRCSync\ZRCSyncServer.exe`
	debug        bool   = false
)

var (
	err errorMsg
)

func main() {
	fmt.Println(":: ZRCSync ::")
	fmt.Println("-----------------------------------------------------------")
	fmt.Println("ATENÇÃO: Após instalação do ZRCSync como serviço,")
	fmt.Println("o mesmo será iniciado automáticamente.")
	fmt.Println("Confirme se todas as opções do ZRCSync.ini estão corretas.")
	fmt.Println("-----------------------------------------------------------")
	fmt.Println(" 1 - Instalar o ZRCSync como serviço.")
	fmt.Println(" 2 - Iniciar o serviço ZRCSync.")
	fmt.Println(" 3 - Parar o serviço ZRCSync.")
	fmt.Println(" 4 - Desinstalar o ZRCSync como serviço.")
	fmt.Println(" 5 - Alterar ficheiros INI.")
	fmt.Println(" 6 - Sair.")
	if debug {
		fmt.Println(" 666 - DEBUG: Remove eventLog")
	}

	fmt.Println("-----------------------------------------------------------")
	fmt.Print(" Escolha uma das opções acima: ")

	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.TrimSpace(input)

	switch input {
	case "1":
		installAndRun()
	case "2":
		run()
	case "3":
		stop()
	case "4":
		remove()
	case "5":
		fmt.Println("A alterar ficheiros INI...")
		time.Sleep(time.Second)
		addIniOptions()
		fmt.Println("Ficheiros INI alterados!")
		enterToExit()
	case "6":
		os.Exit(1)
	case "666":
		removeEventLog(zsrcSyncName)
		enterToExit()
	default:
		fmt.Println("Opção inválida")
	}
}

func enterToExit() {
	fmt.Println()
	fmt.Println("Prima ENTER para terminar...")
	fmt.Scanln()
	os.Exit(1)
}

func installAndRun() {
	fmt.Println()
	fmt.Println("A instalar serviço...")
	result, err := createService(zsrcSyncName, zsrcSyncPath)
	if !result {
		if err.code != serviceExistsCode {
			fmt.Println(prepareErrorMsg(err))
			enterToExit()
		}
	}
	time.Sleep(time.Second)
	fmt.Println("Serviço instalado com sucesso!")

	run()

	fmt.Println()
	fmt.Println("A alterar ficheiros INI...")
	time.Sleep(time.Second)
	addIniOptions()
	fmt.Println("Ficheiros INI alterados!")
	enterToExit()
}

func run() {
	fmt.Println()
	fmt.Println("A iniciar o serviço...")
	result, err := startService(zsrcSyncName)
	if !result {
		if err.err.Error() != serviceRunningMsg {
			fmt.Println(prepareErrorMsg(err))
			enterToExit()
		}
	}
	time.Sleep(time.Second)
	fmt.Println("Serviço iniciado com sucesso!")
}

func stop() {
	fmt.Println()
	fmt.Println("A parar o serviço...")
	result, err := stopService(zsrcSyncName)
	if !result {
		fmt.Println(prepareErrorMsg(err))
		enterToExit()

	}
	time.Sleep(time.Second)
	fmt.Println("Serviço parado com sucesso!")
}

func remove() {
	fmt.Println()
	fmt.Println("A eliminar o serviço...")
	result, err := removeService(zsrcSyncName)
	if !result {
		fmt.Println(prepareErrorMsg(err))
		enterToExit()

	}
	time.Sleep(time.Second)
	fmt.Println("Serviço eliminado com sucesso!")
}
