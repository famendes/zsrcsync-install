package main

const (
	managerConnectionCode int    = 900
	managerConnectionMsg  string = "Não foi possível ligar ao gestor de serviços!"

	serviceAccessCode int    = 901
	serviceAccessMsg  string = "Não foi possível aceder ao serviço."

	startServiceCode int    = 902
	startServiceMsg  string = "Não foi possível iniciar o serviço."

	serviceMissingCode int    = 903
	serviceMissingMsg  string = "O serviço não existe."

	serviceStatusCode int    = 904
	serviceStatusMsg  string = "Falha ao obter estado do serviço."

	stopIncompleteCode int    = 905
	stopIncompleteMsg  string = "Paragem não foi concluída."

	serviceExistsCode int    = 906
	serviceExistsMsg  string = "O serviço já existe."

	cresteServiceCode int = 907
	cresteServiceMsg      = "Erro ao criar serviço."

	removeEventLogSourceCode = 908
	removeEventLogSourceMsg  = "Chamada a RemoveEventLogSource() falhou."

	eventLogCode = 909
	eventLogMsg  = "Erro ao abrir o event log."

	setupEventLogSourceCode int    = 910
	setupEventLogSourceMsg  string = "Chamada a SetupEventLogSource() falhou."

	deleteServiceCode int    = 911
	deleteServiceMsg  string = "Erro ao eliminar serviço."

	serviceRunningCode = 999
	serviceRunningMsg  = "An instance of the service is already running."
)
