package main

import (
	"time"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

func startService(name string) (bool, errorMsg) {
	manager, err := mgr.Connect()
	if err != nil {
		return false, errorMsg{code: managerConnectionCode, msg: managerConnectionMsg, service: name, err: err}
	}
	defer manager.Disconnect()
	service, err := manager.OpenService(name)
	if err != nil {
		return false, errorMsg{code: serviceAccessCode, msg: serviceAccessMsg, service: name, err: err}
	}
	defer service.Close()
	err = service.Start()
	if err != nil {
		return false, errorMsg{code: serviceAccessCode, msg: startServiceMsg, service: name, err: err}
	}
	return true, errorMsg{}
}

func stopService(name string) (bool, errorMsg) {
	manager, err := mgr.Connect()
	if err != nil {
		return false, errorMsg{code: managerConnectionCode, msg: managerConnectionMsg, service: name, err: err}
	}
	defer manager.Disconnect()

	service, err := manager.OpenService(name)
	if err != nil {
		return false, errorMsg{code: serviceMissingCode, msg: serviceMissingMsg, service: name, err: err}
	}
	defer service.Close()

	start := time.Now()
	status, err := service.Control(svc.Stop)
	for time.Since(start) > 5*time.Second {
		status, err = service.Query()
		if err != nil {
			return false, errorMsg{code: serviceStatusCode, msg: serviceStatusMsg, service: name, err: err}
		}

		if status.State == svc.Stopped {
			return true, errorMsg{}
		}
	}
	return false, errorMsg{code: stopIncompleteCode, msg: stopIncompleteMsg}
}

func createService(name, path string) (bool, errorMsg) {
	manager, err := mgr.Connect()
	if err != nil {
		return false, errorMsg{code: managerConnectionCode, msg: managerConnectionMsg, service: name, err: err}
	}
	defer manager.Disconnect()

	service, err := manager.OpenService(name)
	if err == nil {
		service.Close()
		return false, errorMsg{code: serviceExistsCode, msg: serviceExistsMsg, service: name, err: err}
	}
	service, err = manager.CreateService(name, path, mgr.Config{DisplayName: name, StartType: mgr.StartAutomatic}, "is", "auto-started")
	if err != nil {
		return false, errorMsg{code: cresteServiceCode, msg: cresteServiceMsg, service: name, err: err}
	}
	defer service.Close()

	event, err := eventlog.Open(name)
	if event != nil {
		eventlog.Remove(name)
		if err != nil {
			return false, errorMsg{code: removeEventLogSourceCode, msg: removeEventLogSourceMsg, service: name, err: err}
		}
	}
	if err != nil {
		return false, errorMsg{code: eventLogCode, msg: eventLogMsg, service: name, err: err}
	}

	err = eventlog.InstallAsEventCreate(name, eventlog.Error|eventlog.Warning|eventlog.Info)
	if err != nil {
		service.Delete()
		return false, errorMsg{code: setupEventLogSourceCode, msg: setupEventLogSourceMsg, service: name, err: err}
	}
	return true, errorMsg{}
}

func removeService(name string) (bool, errorMsg) {
	manager, err := mgr.Connect()
	if err != nil {
		return false, errorMsg{code: managerConnectionCode, msg: managerConnectionMsg, service: name, err: err}
	}
	defer manager.Disconnect()
	service, err := manager.OpenService(name)
	if err != nil {
		return false, errorMsg{code: serviceMissingCode, msg: serviceMissingMsg, service: name, err: err}
	}
	defer service.Close()
	err = service.Delete()
	if err != nil {
		return false, errorMsg{code: deleteServiceCode, msg: deleteServiceMsg, service: name, err: err}
	}
	err = eventlog.Remove(name)
	if err != nil {
		return false, errorMsg{code: removeEventLogSourceCode, msg: removeEventLogSourceMsg, service: name, err: err}
	}
	return true, errorMsg{}
}

func removeEventLog(name string) (bool, errorMsg) {
	err := eventlog.Remove(name)
	if err != nil {
		return false, errorMsg{code: removeEventLogSourceCode, msg: removeEventLogSourceMsg, service: name, err: err}
	}
	return true, errorMsg{}
}
