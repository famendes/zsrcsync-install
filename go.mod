module zsrcsync-install

go 1.17

require (
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e
	gopkg.in/ini.v1 v1.66.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
