package main

import (
	"fmt"
	"os"

	"gopkg.in/ini.v1"
)

type ZRCSyncOptions struct {
	Reservas      int `ini:"reservas"`
	Pedidos       int `ini:"pedidos"`
	Zeramentos    int `ini:"zeramentos"`
	Historicos    int `ini:"historicos"`
	Colaboradores int `ini:"colaboradores"`
	Carregamentos int `ini:"carregamentos"`
}

type ZSyncServerOptions struct {
	Colaboradores int `ini:"COLABORADORES"`
	Historicos    int `ini:"HISTORICOS"`
	Carregamentos int `ini:"CARREGAMENTOS"`
}

type ZSRestOptions struct {
	ReservasOnline int `ini:"RESERVAS ONLINE"`
	Colaboradores  int `ini:"COLABORADORES"`
}

var (
	syncServerIniPath string = `C:\Zone Soft\ZSRest\ZSyncServer.ini`
	zsrcSyncIniPath   string = `C:\Zone Soft\ZRCSync\ZRCSyncServer.ini`
	zsrestIniPath     string = `C:\Zone Soft\ZSRest\ZSRest.ini`
	iniSection        string = "OPCOES"
	colabKey          string = "COLABORADORES"
	colabValue        string = "0"
	histKey           string = "HISTORICOS"
	histValue         string = "0"
)

func addIniOptions() {
	// cfg, err := ini.Load(syncServerIniPath)
	// if err != nil {
	// 	fmt.Printf("Fail to read file: %v", err)
	// 	os.Exit(1)
	// }
	options := readConfig(zsrcSyncIniPath)
	syncOptions := mapToSyncServer(*options)
	zsrestOptions := mapToZSRest(*options)

	saveToSync(syncOptions)
	saveToZSRest(zsrestOptions)
	// cfg.Section(iniSection).Key(colabKey).SetValue(colabValue)
	// cfg.Section(iniSection).Key(histKey).SetValue(histValue)
	// cfg.SaveTo(syncServerIniPath)
}

func readConfig(path string) *ZRCSyncOptions {
	cfg, err := ini.Load(zsrcSyncIniPath)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}
	op := new(ZRCSyncOptions)
	err = cfg.Section("OPCOES").MapTo(&op)
	if err != nil {
		fmt.Printf("Error reading config file: %v", err)
		os.Exit(1)
	}
	return op
}

func mapToSyncServer(options ZRCSyncOptions) *ZSyncServerOptions {
	ss := new(ZSyncServerOptions)

	if options.Carregamentos != 0 {
		ss.Carregamentos = 0
	}

	if options.Colaboradores != 0 {
		ss.Colaboradores = 0
	}

	if options.Historicos != 0 {
		ss.Historicos = 0
	}
	return ss
}

func mapToZSRest(options ZRCSyncOptions) *ZSRestOptions {
	zsrest := new(ZSRestOptions)

	if options.Reservas != 0 {
		zsrest.ReservasOnline = 0
	}

	if options.Colaboradores != 0 {
		zsrest.Colaboradores = 0
	}

	return zsrest
}

func saveToSync(options *ZSyncServerOptions) {
	ini.PrettyFormat = false
	cfg, err := ini.Load(syncServerIniPath)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	iniSection := cfg.Section("OPCOES")

	if iniSection.HasKey("colaboradores") {
		iniSection.DeleteKey("colaboradores")
	}

	if iniSection.HasKey("historicos") {
		iniSection.DeleteKey("historicos")
	}

	if iniSection.HasKey("carregamentos") {
		iniSection.DeleteKey("carregamentos")
	}

	iniSection.ReflectFrom(options)
	cfg.SaveTo(syncServerIniPath)
}

func saveToZSRest(options *ZSRestOptions) {
	ini.PrettyFormat = false
	cfg, err := ini.Load(zsrestIniPath)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	iniSection := cfg.Section("OPCOES")

	if iniSection.HasKey("colaboradores") {
		iniSection.DeleteKey("colaboradores")
	}

	if iniSection.HasKey("reservas online") {
		iniSection.DeleteKey("reservas online")
	}

	cfg.Section("OPCOES").ReflectFrom(options)
	cfg.SaveTo(zsrestIniPath)
}
