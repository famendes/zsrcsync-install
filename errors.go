package main

import "fmt"

type errorMsg struct {
	code    int
	msg     string
	service string
	err     error
}

func prepareErrorMsg(err errorMsg) string {
	fmt.Println()
	msg := fmt.Sprintf("Código: %v \nMensagem: %v \nServiço: %v \nErro: %v", err.code, err.msg, err.service, err.err)
	return msg
}
